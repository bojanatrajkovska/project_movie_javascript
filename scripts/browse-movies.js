let pageNumber = 1
let upperLimit = 8;
let totalNumberOfPages;

$(document).ready(function () {

    const params = new URLSearchParams(window.location.search);
    const movie_name = params.get('movieName');
    document.getElementById('keyword-input').value = movie_name;

    console.log('Params: ', movie_name)
    $('#browseForm').submit(function () {
        $.ajax({
            url: 'https://yts.mx/api/v2/list_movies.json',
            type: 'GET',
            data: $('#browseForm').serialize() + '&page=' + pageNumber,
            success: function (response) {
                console.log('DATA: ', $('#browseForm').serialize());
                console.log('response: ', response);

                document.getElementById("movieCount").innerText = response.data.movie_count;

                document.getElementById("cardWrapper").innerHTML = '';
                createCards('cardWrapper', response.data.movies);
                CreatePaginator(response.data.movie_count);
            }
        });

        
        return false;
    });

    $('#browseForm').submit();
    // CreatePaginator(80000);
});

function Search() {
    pageNumber = 1;
    $('#browseForm').submit()
}

function CreateButton(index, location){
    let btn = document.createElement('button');
    btn.classList.add('btn');
    btn.classList.add('btn-outline-success');
    btn.classList.add('pagination-buttons');

    if(index == pageNumber){
        btn.classList.add('active-button');
    }
    btn.innerText = index;
    btn.id = 'btn-' + location + '-' + index;
    
    btn.addEventListener('click',
        function () {
            GoToPage(index);
        }
    );

    let paginationDiv = document.getElementById('pagination-' + location);
    paginationDiv.append(btn);
}

function CreateBreakpoint(location){
    let btn = document.createElement("span");
    btn.classList.add('btn');
    btn.classList.add('btn-outline-success');
    btn.classList.add('pagination-buttons');
    btn.innerText = "...";

    let paginationDiv = document.getElementById('pagination-' + location);
    paginationDiv.append(btn);
}

function CreatePaginator(movie_count) {
    document.getElementById('pagination-top').innerHTML ='';
    document.getElementById('pagination-bottom').innerHTML ='';
    let numberOfButtons = Math.ceil(movie_count / 20);

    totalNumberOfPages = numberOfButtons;

    for (let i = 1; i <= numberOfButtons; i++) {

        if(pageNumber >= upperLimit){
            //Create buttons for the first 2 pages, 
            //                       pages from pageNumber - 3 to pageNumber + 3
            //                       last 2 pages
            if(i <= 2 
                || (i >= (pageNumber - 3) && i <= (pageNumber + 3))
                || i >= numberOfButtons - 1){
                CreateButton(i, 'top');
                CreateButton(i, 'bottom');
            }
            else if (i == 3 || i == pageNumber + 4) {
                CreateBreakpoint('top');
                CreateBreakpoint('bottom');
            }
        }
        else{
            if (i <= upperLimit || i >= numberOfButtons - 1) {
                CreateButton(i, 'top');
                CreateButton(i, 'bottom');
            }
            else if (i == upperLimit + 1) {
                CreateBreakpoint('top');
                CreateBreakpoint('bottom');
            }
        }
    }
    HideButtons(numberOfButtons, 'top');
    HideButtons(numberOfButtons, 'bottom');
}

function HideButtons(numberOfButtons, location) {
    if (pageNumber == 1) {
        document.getElementById('first-' + location).style.display = 'none';
        document.getElementById('previous-' + location).style.display = 'none';
    }else{
        document.getElementById('first-' + location).style.display = 'inline-block';
        document.getElementById('previous-' + location).style.display = 'inline-block';
    }

    if (pageNumber == numberOfButtons) {
        document.getElementById('last-' + location).style.display = 'none';
        document.getElementById('next-' + location).style.display = 'none';
    }else{
        document.getElementById('last-' + location).style.display = 'inline-block';
        document.getElementById('next-' + location).style.display = 'inline-block';
    }
}

function GoToPage(num) {
    pageNumber = num;
    $('#browseForm').submit();
}

function GoToNextPage() {
    pageNumber = pageNumber + 1;
    $('#browseForm').submit();
}

function GoToPreviousPage() {
    pageNumber = pageNumber - 1;
    $('#browseForm').submit();
}

function GoToLastPage() {
    pageNumber = totalNumberOfPages;
    $('#browseForm').submit();
}

function GoToFirstPage() {
    pageNumber = 1;
    $('#browseForm').submit();
}