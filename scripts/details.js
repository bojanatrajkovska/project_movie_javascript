let urlParameters = new URLSearchParams(window.location.search);
let movie_id = urlParameters.get('movie_id');

getMovie(movie_id);
getSuggestedMovies(movie_id);

function getMovie(movie_id) {

    let movie;
    let request = new XMLHttpRequest();

    request.open('GET', 'https://yts.mx/api/v2/movie_details.json?movie_id=' + movie_id);

    request.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            movie = JSON.parse(this.response).data.movie;
            document.title = movie.title;
            document.getElementById('image-holder').style.backgroundImage = "url(" + movie.background_image + ")";
            console.log(movie);
            
            GetTrailer(movie);
            SetTorrents(movie);
        }
        else if (this.status == 404) {
            alert('URL not found!')
        }
    };

    request.send();
}

function createTorrentCard(torrent, index) {

    let torrentHolder = document.getElementById('torrent-wrapper');

    //Creating a div for the card holder
    let cardHolder = document.createElement('div');
    cardHolder.classList.add('col-4');
    cardHolder.classList.add('torrent-card');
    if ((index + 1) % 3 != 0) {
        cardHolder.classList.add('border-right');
    }

    let torrentType = document.createElement('h6');
    torrentType.classList.add('torrent-type');
    torrentType.innerText = torrent.type.toUpperCase();

    let fileSize = document.createElement('h6');
    fileSize.innerText = 'File size';

    let torrentSize = document.createElement('h6');
    torrentSize.classList.add('torrent-size');
    torrentSize.innerText = torrent.size;

    //Creating the view details button
    let downloadBtn = document.createElement('a');
    downloadBtn.classList.add("btn");
    downloadBtn.classList.add("btn-success");

    downloadBtn.innerText = "Download";
    downloadBtn.setAttribute('href', torrent.url);

    downloadBtn.addEventListener('click', closeModal);

    let icon = document.createElement('i');
    icon.classList.add("fas");
    icon.classList.add("fa-download");
    downloadBtn.append(icon);

    cardHolder.append(torrentType);
    cardHolder.append(fileSize);
    cardHolder.append(torrentSize);
    cardHolder.append(downloadBtn);

    torrentHolder.append(cardHolder);
}

function downloadTorrent(url) {
    window.open(url);
}

function closeModal() {
    $('#downloadModal').modal('hide');
}

function getSuggestedMovies(movie_id) {
    ;

    let request = new XMLHttpRequest();

    request.open('GET', 'https://yts.mx/api/v2/movie_suggestions.json?movie_id=' + movie_id);

    request.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            let suggestedMovies = JSON.parse(this.response).data.movies;

            console.log('Suggested movies', suggestedMovies);

            for (let i = 0; i < suggestedMovies.length; i++) {
                let suggested = document.getElementById('suggested-' + (i + 1));
                let suggestedLink = document.getElementById('s-link-' + (i + 1));

                if (suggested != null && suggested != undefined) {
                    console.log(suggestedMovies[i].medium_cover_image);
                    suggested.setAttribute('src', suggestedMovies[i].medium_cover_image)
                    suggestedLink.setAttribute('href', 'moviedetails.html?movie_id=' + suggestedMovies[i].id)
                }
            }
        }
        else if (this.status == 404) {
            alert('URL not found!')
        }
    };

    request.send();
}

function GetTrailer(movie) {
    document.getElementById("movie-image").setAttribute("src", movie.large_cover_image);
    document.getElementById("movie-title").innerText = movie.title;
    document.getElementById("movie-year").innerText = movie.year;
    document.getElementById('movie-trailer').setAttribute('src', 'https://www.youtube.com/embed/' + movie.yt_trailer_code);
    document.getElementById('movie-img-1').setAttribute('src', 'https://img.youtube.com/vi/' + movie.yt_trailer_code + '/1.jpg');
    document.getElementById('movie-img-2').setAttribute('src', 'https://img.youtube.com/vi/' + movie.yt_trailer_code + '/2.jpg');
}

function SetGenres(movie) {
    let movieGenres = document.getElementById("movie-genres");

    for (let i = 0; i < movie.genres.length; i++) {
        movieGenres.innerText += (i > 0 ? ' / ' : '') + movie.genres[i];
    }
}

function SetTorrents(movie) {

    let mainContent = document.getElementById("main-content");

    movie.torrents.forEach((torrent, index) => {
        let downloadItem = document.createElement('div');
        downloadItem.classList.add('col-2', 'download-item');

        let link = document.createElement('a');
        link.setAttribute('href', torrent.url);
        link.innerText = torrent.quality;
        downloadItem.append(link);

        mainContent.append(downloadItem);

        createTorrentCard(torrent, index);
    });
}

function searchMovie(){
    let searchText = document.getElementById("headerSearch").value;
    window.location = "browse.html?movieName=" + searchText;
}