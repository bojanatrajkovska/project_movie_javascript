function getMovies(){
   let movies; 
   let request = new XMLHttpRequest();

   request.open('GET', 'https://yts.mx/api/v2/list_movies.json');

   request.onreadystatechange = function(){
       if(this.readyState == 4 && this.status == 200){
           movies = JSON.parse(this.response).data.movies;
           createCards('homepage-cards', movies);
       }
       else if(this.status == 404){
           alert('URL not found!')
       }
   };

    request.send();
}
getMovies();